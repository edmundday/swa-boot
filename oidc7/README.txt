OAuth2 Proof of Concept - HPE
=======================

To compile:

- go to each directory:
   test-oauth2-authserver/
   test-oauth2-client/
   test-oauth2-resource/
   test-oauth2-resource2/
  and run:
   mvn clean package

To run:

- go to each directory as above (in a separate terminal window) and run each jar:
  
  cd C:\Users\ewd5\Documents\C3ISP\software\mirko oauth\test-oauth2-resource
  java -jar target/test-oauth2-resource-0.0.1-SNAPSHOT.jar    [port=7777]

  cd C:\Users\ewd5\Documents\C3ISP\software\mirko oauth\test-oauth2-resource2
  java -jar target/test-oauth2-resource2-0.0.1-SNAPSHOT.jar   [port=7778]

  cd C:\Users\ewd5\Documents\C3ISP\software\mirko oauth\test-oauth2-client
  java -jar target/test-oauth2-client-0.0.1-SNAPSHOT.jar      [port=8888]

  cd C:\Users\ewd5\Documents\C3ISP\software\mirko oauth\test-oauth2-authserver
  java -jar target/test-oauth2-0.0.1-SNAPSHOT.jar             [port=9999]


To see how it works:

- Connect via a browser to http://localhost:8888/client/
  The flow is described in the slideset: 
  . The /client/ is a protected service (running on port 8888) that when you authenticate, it will issue a REST call to the resource and resource2 servers endpoints (two REST services running on port 7777 and 7778).
  . Before giving access to the resources (port 7777, 7778), it will ask for authentication on the authserver (port 9999)
  . Credentials are: user/password and admin/password
  . The auth server releases a rf4bgtz\hbfVEFVDEUJM OAuth2 token encrypted with a symmetric key (my_key) configured in the auth server
  . The /client/ then calls the two resources, one after the other, passing the JWT
  . Since the JWT is recognised by both resources, authentication is not needed anymore
  . The flow employs the OAuth2 protocol as provided by Spring Boot 1.3.x.

