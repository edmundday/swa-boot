/**
 * Copyright 2016 Hewlett Packard Enterprise Development Company, L.P.
 */
package com.hpe.c3isp.oauth2;

import java.util.Arrays;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author MIMANE
 *
 */
@SpringBootApplication
//@EnableOAuth2Client
@EnableOAuth2Sso
@RestController
public class ClientApplication {
    
  @Value("${oidc.clientId}")
  private String clientId;

  @Value("${oidc.clientSecret}")
  private String clientSecret;

  @Value("${oidc.accessTokenUri}")
  private String accessTokenUri;

  @Value("${oidc.userAuthorizationUri}")
  private String userAuthorizationUri;

  @Value("${oidc.redirectUri}")
  private String redirectUri;
  
  public OAuth2ProtectedResourceDetails OpenId() {
      final AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
      details.setClientId(clientId);
      details.setClientSecret(clientSecret);
      details.setAccessTokenUri(accessTokenUri);
      details.setUserAuthorizationUri(userAuthorizationUri);
      details.setScope(Arrays.asList("openid", "email"));
      details.setPreEstablishedRedirectUri(redirectUri);
      details.setUseCurrentUri(false);
      return details;
  }
    
  @Bean
  public OAuth2RestOperations restTemplate(OAuth2ClientContext oauth2ClientContext) {
      return new OAuth2RestTemplate(OpenId(), oauth2ClientContext);
  }
    
    public static void main(String[] args) {
        //SpringApplication.run(AuthserverApplication.class, args);
        new SpringApplicationBuilder(ClientApplication.class)
        .properties("spring.config.name=client").run(args);
    }

    @Autowired
    private OAuth2RestOperations restTemplate;

    @Value("${c3isp.resource.publish-uri}")
    private String resourcePublishURI;
    
    @Value("${c3isp.resource.read-uri}")
    private String resourceReadURI;

    @RequestMapping("/")
    //public JsonNode home() {
    public String home() {
        String str;
        str = "<b>1st RESOURCE</b>: "+restTemplate.getForObject(resourcePublishURI, String.class);
        str += "<br/><br/><b>2nd RESOURCE</b>: "+restTemplate.getForObject(resourceReadURI, String.class);
        return str;
        //return restTemplate.getForObject(resourcePublishURI, JsonNode.class);
    }
    
}
